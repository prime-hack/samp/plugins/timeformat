#ifndef TimeFormat_H
#define TimeFormat_H

#include "../base/d3d9/d3drender.h"
#include "../base/d3d9/imgui/imgui.h"
#include "../base/d3d9/imgui_expand.h"
#include "../base/d3d9/notifications.h"
#include "../base/d3d9/texture.h"
#include "loader/loader.h"

#include "../base/d3d9/ImWrapper/ImWrapper.hpp"

#include "../FileMan/ciniserialization.h"

class TimeFormat : public SRDescent, public CIniSerialization {
	CCallHook *_hookSpaceMessages;
	CCallHook *_hookSpaceUserMessages;

	SERIALIZE_EXT( std::string, format, "[%H:%M:%S]" )
	SERIALIZE_EXT( int, space, 50 )

	ImMenu *_menu;
	// Поля сюда вынесены, для обновления значений из команды.
	ImInputText *_editFormat;
	ImInputInt *_editSpace;

public:
	explicit TimeFormat();
	virtual ~TimeFormat();

	void command( char * );

private:
	void ChatInfo_Render_spaceMessagesTime();
	void ChatInfo_Render_spaceUserMessagesTime();
};

#endif // TimeFormat_H
