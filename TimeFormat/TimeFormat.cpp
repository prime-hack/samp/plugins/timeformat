#include "TimeFormat.h"
#include "../base/CGame/methods.h"
#include <chrono>

TimeFormat::TimeFormat() : SRDescent(), CIniSerialization( PROJECT_NAME + ".ini" ) {
	// Хуки записи смещения чата относительно времени, без хуков это значение можно изменять только в
	// диапозоне [-128; +127]
	_hookSpaceMessages = new CCallHook( (void *)( g_handle.dwSAMP + 0x63EAF ), 9, 0, cp_before );
	_hookSpaceMessages->enable( this, &TimeFormat::ChatInfo_Render_spaceMessagesTime );
	_hookSpaceUserMessages = new CCallHook( (void *)( g_handle.dwSAMP + 0x63F10 ), 9, 0, cp_before );
	_hookSpaceUserMessages->enable( this, &TimeFormat::ChatInfo_Render_spaceUserMessagesTime );

	// Применение формата времени
	memsafe::write<size_t>( (void *)( g_handle.dwSAMP + 0x63E2D ), (size_t)format().data() );

	// Создание ImGui меню
	_menu = new ImMenu( PROJECT_NAME );
	_menu->flags |=
		ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoCollapse;
		_menu->onHide += []() { g_class.cursor->hideCursor(); };

	// ImGui поле редактирования формата времени
	_editFormat = new ImInputText( "format", format(), 64 );
	_editFormat->onTextChanged += [this]( std::string text ) {
		format() = text;
		memsafe::write<size_t>( (void *)( g_handle.dwSAMP + 0x63E2D ), (size_t)format().data() );
		g_class.samp->chat()->update();
	};

	// ImGui поле редактирования отступа
	_editSpace = new ImInputInt( "space", space() );
	_editSpace->fastStep = 10;
	_editSpace->onIntChanged += [this]( int value ) {
		space() = value;
		g_class.samp->chat()->update();
	};

	// Запись полей в меню
	_menu->chields.push_back( _editFormat );
	_menu->chields.push_back( _editSpace );

	// Регистрация команды для изменения формата времени
	g_class.samp->input()->addChatCommand( PROJECT_NAME.toLower(), this, &TimeFormat::command );

	SF::Instance()->Log( "{FF00FF00}Plugin %s has loaded", PROJECT_NAME.c_str() );
}

TimeFormat::~TimeFormat() {
	g_class.samp->input()->deleteCommand( PROJECT_NAME.toLower() );

	for ( auto &item : _menu->chields ) delete item;
	delete _menu;

	memsafe::write<size_t>( (void *)( g_handle.dwSAMP + 0x63E2D ), g_handle.dwSAMP + 0xD7988 );

	delete _hookSpaceUserMessages;
	delete _hookSpaceMessages;

	SF::Instance()->Log( "{FFFF0000}Plugin %s has unloaded", PROJECT_NAME.c_str() );
}

void TimeFormat::command( char *text ) {
	auto isDigit = []( const char *text ) {
		auto len = strlen( text );
		for ( uint i = 0; i < len; ++i ) {
			if ( text[i] == '-' && i == 0 ) continue;
			if ( text[i] >= '0' && text[i] <= '9' ) continue;
			return false;
		}
		return true;
	};

	if ( strlen( text ) ) {
		if ( isDigit( text ) ) { // Если в команду передано число, то это отступ
			space() = std::stoi( text );
			_editSpace->setValue( space() );
		} else { // Если в команду передона строка, то это формат
			format() = text;
			_editFormat->setText( format() );
		}
		g_class.samp->chat()->update();
	} else { // Если в команду ничего не передано, то открываем/закрываем меню
		g_class.cursor->toggleCursor( g_class.cursor->isLocalCursorHiden() );
		_menu->toggle( g_class.cursor->isLocalCursorShowed() );
	}
}

void TimeFormat::ChatInfo_Render_spaceMessagesTime() {
	_hookSpaceMessages->setReg86( r86::EBP, _hookSpaceMessages->reg86( r86::EBP ) + space() - 50 );
}

void TimeFormat::ChatInfo_Render_spaceUserMessagesTime() {
	_hookSpaceUserMessages->setReg86( r86::EBP, _hookSpaceUserMessages->reg86( r86::EBP ) + space() - 50 );
}
