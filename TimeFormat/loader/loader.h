#ifndef LOADER_H
#define LOADER_H

#include "../../FileMan/cfilebase.h"
#include "../../FileMan/cfilebin.h"
#include "../../FileMan/cfileinfo.h"
#include "../../FileMan/cfileini.h"
#include "../../FileMan/cfiletext.h"
#include "../../base/base.hpp"
#include "../../sampfuncs/sampfuncs.h"

/**
 * \brief Выводит окно с сообщениемю
 * \detail Является оберткой над MessageBoxA, для более удобного вывода сообщений.
 * \param[in] text Текст сообщения.
 * \param[in] title Заголовок окна с сообщением.
 * \param[in] type Тип окна.
 * \return код завершения окна (нажатая кнопка).
 */
int MessageBox( SRString text, SRString title = PROJECT_NAME, UINT type = MB_OK );

#endif // LOADER_H
